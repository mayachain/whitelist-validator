package validator

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/edx04/validator/x/validator/keeper"
	"github.com/edx04/validator/x/validator/types"
)

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	// Set all the whitelistValidator
	for _, elem := range genState.WhitelistValidatorList {
		k.SetWhitelistValidator(ctx, elem)
	}

	// Set whitelistValidator count
	k.SetWhitelistValidatorCount(ctx, genState.WhitelistValidatorCount)
	// this line is used by starport scaffolding # genesis/module/init
	k.SetParams(ctx, genState.Params)
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()
	genesis.Params = k.GetParams(ctx)

	genesis.WhitelistValidatorList = k.GetAllWhitelistValidator(ctx)
	genesis.WhitelistValidatorCount = k.GetWhitelistValidatorCount(ctx)
	// this line is used by starport scaffolding # genesis/module/export

	return genesis
}
