package validator_test

import (
	"testing"

	keepertest "github.com/edx04/validator/testutil/keeper"
	"github.com/edx04/validator/testutil/nullify"
	"github.com/edx04/validator/x/validator"
	"github.com/edx04/validator/x/validator/types"
	"github.com/stretchr/testify/require"
)

func TestGenesis(t *testing.T) {
	genesisState := types.GenesisState{
		Params: types.DefaultParams(),

		WhitelistValidatorList: []types.WhitelistValidator{
			{
				Id: 0,
			},
			{
				Id: 1,
			},
		},
		WhitelistValidatorCount: 2,
		// this line is used by starport scaffolding # genesis/test/state
	}

	k, ctx := keepertest.ValidatorKeeper(t)
	validator.InitGenesis(ctx, *k, genesisState)
	got := validator.ExportGenesis(ctx, *k)
	require.NotNil(t, got)

	nullify.Fill(&genesisState)
	nullify.Fill(got)

	require.ElementsMatch(t, genesisState.WhitelistValidatorList, got.WhitelistValidatorList)
	require.Equal(t, genesisState.WhitelistValidatorCount, got.WhitelistValidatorCount)
	// this line is used by starport scaffolding # genesis/test/assert
}
