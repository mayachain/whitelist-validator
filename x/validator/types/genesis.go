package types

import (
	"fmt"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	return &GenesisState{
		WhitelistValidatorList: []WhitelistValidator{},
		// this line is used by starport scaffolding # genesis/types/default
		Params: DefaultParams(),
	}
}

// Validate performs basic genesis state validation returning an error upon any
// failure.
func (gs GenesisState) Validate() error {
	// Check for duplicated ID in whitelistValidator
	whitelistValidatorIdMap := make(map[uint64]bool)
	whitelistValidatorCount := gs.GetWhitelistValidatorCount()
	for _, elem := range gs.WhitelistValidatorList {
		if _, ok := whitelistValidatorIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for whitelistValidator")
		}
		if elem.Id >= whitelistValidatorCount {
			return fmt.Errorf("whitelistValidator id should be lower or equal than the last id")
		}
		whitelistValidatorIdMap[elem.Id] = true
	}
	// this line is used by starport scaffolding # genesis/types/validate

	return gs.Params.Validate()
}
