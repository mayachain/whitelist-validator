package keeper_test

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	keepertest "github.com/edx04/validator/testutil/keeper"
	"github.com/edx04/validator/testutil/nullify"
	"github.com/edx04/validator/x/validator/keeper"
	"github.com/edx04/validator/x/validator/types"
	"github.com/stretchr/testify/require"
)

func createNWhitelistValidator(keeper *keeper.Keeper, ctx sdk.Context, n int) []types.WhitelistValidator {
	items := make([]types.WhitelistValidator, n)
	for i := range items {
		items[i].Id = keeper.AppendWhitelistValidator(ctx, items[i])
	}
	return items
}

func TestWhitelistValidatorGet(t *testing.T) {
	keeper, ctx := keepertest.ValidatorKeeper(t)
	items := createNWhitelistValidator(keeper, ctx, 10)
	for _, item := range items {
		got, found := keeper.GetWhitelistValidator(ctx, item.Id)
		require.True(t, found)
		require.Equal(t,
			nullify.Fill(&item),
			nullify.Fill(&got),
		)
	}
}

func TestWhitelistValidatorRemove(t *testing.T) {
	keeper, ctx := keepertest.ValidatorKeeper(t)
	items := createNWhitelistValidator(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveWhitelistValidator(ctx, item.Id)
		_, found := keeper.GetWhitelistValidator(ctx, item.Id)
		require.False(t, found)
	}
}

func TestWhitelistValidatorGetAll(t *testing.T) {
	keeper, ctx := keepertest.ValidatorKeeper(t)
	items := createNWhitelistValidator(keeper, ctx, 10)
	require.ElementsMatch(t,
		nullify.Fill(items),
		nullify.Fill(keeper.GetAllWhitelistValidator(ctx)),
	)
}

func TestWhitelistValidatorCount(t *testing.T) {
	keeper, ctx := keepertest.ValidatorKeeper(t)
	items := createNWhitelistValidator(keeper, ctx, 10)
	count := uint64(len(items))
	require.Equal(t, count, keeper.GetWhitelistValidatorCount(ctx))
}
