package keeper_test

import (
	"testing"

	testkeeper "github.com/edx04/validator/testutil/keeper"
	"github.com/edx04/validator/x/validator/types"
	"github.com/stretchr/testify/require"
)

func TestGetParams(t *testing.T) {
	k, ctx := testkeeper.ValidatorKeeper(t)
	params := types.DefaultParams()

	k.SetParams(ctx, params)

	require.EqualValues(t, params, k.GetParams(ctx))
}
