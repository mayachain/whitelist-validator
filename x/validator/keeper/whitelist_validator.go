package keeper

import (
	"encoding/binary"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/edx04/validator/x/validator/types"
)

// GetWhitelistValidatorCount get the total number of whitelistValidator
func (k Keeper) GetWhitelistValidatorCount(ctx sdk.Context) uint64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.WhitelistValidatorCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	return binary.BigEndian.Uint64(bz)
}

// SetWhitelistValidatorCount set the total number of whitelistValidator
func (k Keeper) SetWhitelistValidatorCount(ctx sdk.Context, count uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), []byte{})
	byteKey := types.KeyPrefix(types.WhitelistValidatorCountKey)
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, count)
	store.Set(byteKey, bz)
}

// AppendWhitelistValidator appends a whitelistValidator in the store with a new id and update the count
func (k Keeper) AppendWhitelistValidator(
	ctx sdk.Context,
	whitelistValidator types.WhitelistValidator,
) uint64 {
	// Create the whitelistValidator
	count := k.GetWhitelistValidatorCount(ctx)

	// Set the ID of the appended value
	whitelistValidator.Id = count

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WhitelistValidatorKey))
	appendedValue := k.cdc.MustMarshal(&whitelistValidator)
	store.Set(GetWhitelistValidatorIDBytes(whitelistValidator.Id), appendedValue)

	// Update whitelistValidator count
	k.SetWhitelistValidatorCount(ctx, count+1)

	return count
}

// SetWhitelistValidator set a specific whitelistValidator in the store
func (k Keeper) SetWhitelistValidator(ctx sdk.Context, whitelistValidator types.WhitelistValidator) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WhitelistValidatorKey))
	b := k.cdc.MustMarshal(&whitelistValidator)
	store.Set(GetWhitelistValidatorIDBytes(whitelistValidator.Id), b)
}

// GetWhitelistValidator returns a whitelistValidator from its id
func (k Keeper) GetWhitelistValidator(ctx sdk.Context, id uint64) (val types.WhitelistValidator, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WhitelistValidatorKey))
	b := store.Get(GetWhitelistValidatorIDBytes(id))
	if b == nil {
		return val, false
	}
	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemoveWhitelistValidator removes a whitelistValidator from the store
func (k Keeper) RemoveWhitelistValidator(ctx sdk.Context, id uint64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WhitelistValidatorKey))
	store.Delete(GetWhitelistValidatorIDBytes(id))
}

// GetAllWhitelistValidator returns all whitelistValidator
func (k Keeper) GetAllWhitelistValidator(ctx sdk.Context) (list []types.WhitelistValidator) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.WhitelistValidatorKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.WhitelistValidator
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

// GetWhitelistValidatorIDBytes returns the byte representation of the ID
func GetWhitelistValidatorIDBytes(id uint64) []byte {
	bz := make([]byte, 8)
	binary.BigEndian.PutUint64(bz, id)
	return bz
}

// GetWhitelistValidatorIDFromBytes returns ID in uint64 format from a byte array
func GetWhitelistValidatorIDFromBytes(bz []byte) uint64 {
	return binary.BigEndian.Uint64(bz)
}
