package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/cosmos/cosmos-sdk/types/query"
	"github.com/edx04/validator/x/validator/types"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (k Keeper) WhitelistValidatorAll(c context.Context, req *types.QueryAllWhitelistValidatorRequest) (*types.QueryAllWhitelistValidatorResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var whitelistValidators []types.WhitelistValidator
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	whitelistValidatorStore := prefix.NewStore(store, types.KeyPrefix(types.WhitelistValidatorKey))

	pageRes, err := query.Paginate(whitelistValidatorStore, req.Pagination, func(key []byte, value []byte) error {
		var whitelistValidator types.WhitelistValidator
		if err := k.cdc.Unmarshal(value, &whitelistValidator); err != nil {
			return err
		}

		whitelistValidators = append(whitelistValidators, whitelistValidator)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllWhitelistValidatorResponse{WhitelistValidator: whitelistValidators, Pagination: pageRes}, nil
}

func (k Keeper) WhitelistValidator(c context.Context, req *types.QueryGetWhitelistValidatorRequest) (*types.QueryGetWhitelistValidatorResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	ctx := sdk.UnwrapSDKContext(c)
	whitelistValidator, found := k.GetWhitelistValidator(ctx, req.Id)
	if !found {
		return nil, sdkerrors.ErrKeyNotFound
	}

	return &types.QueryGetWhitelistValidatorResponse{WhitelistValidator: whitelistValidator}, nil
}
